/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYELEMENTAL__TABLE_HH
#define PYELEMENTAL__TABLE_HH

#include "element.hh"
#include <libelemental/table.hh>

PyMODINIT_FUNC initElemental (void);

namespace pyElemental {

//******************************************************************************

class
the_module
{
public:

	static bool ready ();

private:

	static PyObject* wrap_table ();

	static PyMethodDef methods[];

	static PyObject* get_element (PyObject*, PyObject* args);
};

} // namespace pyElemental

#endif // PYELEMENTAL__TABLE_HH

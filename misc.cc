/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "misc.hh"


bool
X_PyType_AddIntConstant (PyTypeObject* type, const char* name, long value)
{
	return PyDict_SetItemString (X_PYOBJECT (type->tp_dict),
		name, PyInt_FromLong (value)) == 0;
}


bool
X_PyObject_CheckAttr (PyObject* attr, PyTypeObject* attrtype,
	const char* attrname, PyTypeObject* type)
{
	if (attr == NULL)
	{
		PyErr_Format (PyExc_TypeError, "cannot delete a %s %s",
			type->tp_name, attrname);
		return false;
	}
	else if (!PyObject_TypeCheck (attr, attrtype))
	{
		PyErr_Format (PyExc_TypeError, "A %s %s must be a(n) %s.",
			type->tp_name, attrname, attrtype->tp_name);
		return false;
	}
	else
		return true;
}


bool
X_PySequence_CheckItems (PyObject* seq, PyTypeObject* type)
{
	if (!PySequence_Check (seq)) return false;
	int size = PySequence_Size (seq);
	if (size < 0) return false;
	
	for (int i = 0; i < size; ++i)
	{
		PyObject *item = PySequence_GetItem (seq, i);
		if (item == NULL) return false;
		bool item_valid = PyObject_TypeCheck (item, type);
		Py_DECREF (item);
		if (!item_valid) return false;
	}
	
	return true;
}


PyObject*
X_PyString_FromCxxString (const std::string& source)
{
	return PyString_FromString (source.data ());
}


PyObject*
X_PyUnicode_FromUstring (const Glib::ustring& source)
{
	return PyUnicode_DecodeUTF8 (source.data (), source.raw ().size (), NULL);
}


Glib::ustring
X_PyUnicode_AsUstring (PyObject* self)
{
	if (self == NULL) return Glib::ustring ();
	PyObject *str = PyUnicode_AsUTF8String (self);
	if (str == NULL) return Glib::ustring ();
	Glib::ustring result = PyString_AsString (str);
	Py_DECREF (str);
	return result;
}

<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE xsl:stylesheet [
	<!ENTITY nbsp "&#160;">
	<!ENTITY mdash "&#x2014;">
]>
<!--
	DocBook stylesheet for pyElemental.
	Originally adapted from PyGObject's docs/xsl/html.xsl.

	Copyright (C) 2007 Kevin Daughtridge <kevin@kdau.com>
	Copyright (C) Johan Dahlin, James Henstridge (PyGObject)

	This document is free software: you can redistribute it and/or modify it
	under the terms of the GNU General Public License as published by the Free
	Software Foundation, either version 3 of the License, or (at your option)
	any later version.
	
	This document is distributed in the hope that it will be useful, but WITHOUT
	ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
	FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for
	more details.
	
	You should have received a copy of the GNU General Public License along with
	this document. If not, see <http://www.gnu.org/licenses/>.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

	<xsl:import href="http://docbook.sourceforge.net/release/xsl/current/xhtml/chunk.xsl" />

	<xsl:param name="chunker.output.encoding" select="'utf-8'" />
	<xsl:param name="generate.legalnotice.link" select="1" />
	<xsl:param name="use.id.as.filename" select="1" />
	<xsl:param name="variablelist.term.break.after" select="1" />
	<xsl:param name="variablelist.term.separator" select="''" />

	<xsl:template match="classsynopsis|constructorsynopsis|destructorsynopsis|methodsynopsis|fieldsynopsis">
		<xsl:param name="language">
			<xsl:choose>
				<xsl:when test="@language"><xsl:value-of select="@language" /></xsl:when>
				<xsl:otherwise><xsl:value-of select="$default-classsynopsis-language" /></xsl:otherwise>
			</xsl:choose>
		</xsl:param>
		<xsl:choose>
			<xsl:when test="$language='python'"><xsl:apply-templates select="." mode="python" /></xsl:when>
			<xsl:otherwise><xsl:apply-imports /></xsl:otherwise>
		</xsl:choose>
	</xsl:template>

	<xsl:template match="classsynopsis" mode="python">
		<pre class="{name(.)}">
			<xsl:text>class&nbsp;</xsl:text>
			<xsl:apply-templates select="ooclass[1]" mode="python" />
			<xsl:if test="ooclass[position() &gt; 1]">
				<xsl:text>&nbsp;(</xsl:text>
				<xsl:apply-templates select="ooclass[position() &gt; 1]" mode="python" />
				<xsl:text>)</xsl:text>
			</xsl:if>
			<xsl:text>:</xsl:text>
			<xsl:apply-templates select="constructorsynopsis|destructorsynopsis|methodsynopsis|fieldsynopsis|classsynopsisinfo" mode="python" />
		</pre>
	</xsl:template>

	<xsl:template match="constructorsynopsis" mode="python">
		<code class="{name(.)}">
			<xsl:apply-templates select="methodname" mode="python" />
			<xsl:text>&nbsp;(</xsl:text>
			<xsl:apply-templates select="methodparam" mode="python" />
			<xsl:text>)</xsl:text>
		</code>
	</xsl:template>

	<xsl:template match="destructorsynopsis|methodsynopsis" mode="python">
		<code class="{name(.)}">
			<xsl:apply-templates select="type|void" mode="python" />
			<xsl:if test="not(child::type or child::void)"><xsl:text>def&nbsp;</xsl:text></xsl:if>
			<xsl:apply-templates select="methodname" mode="python" />
			<xsl:text>&nbsp;(</xsl:text>
			<xsl:apply-templates select="methodparam" mode="python" />
			<xsl:text>)</xsl:text>
		</code>
	</xsl:template>

	<xsl:template match="fieldsynopsis" mode="python">
		<code class="{name(.)}"><xsl:apply-templates mode="python" /></code>
		<xsl:if test="descendant::modifier[text()='const']">&nbsp;&mdash;&nbsp;<em>read-only</em></xsl:if>
	</xsl:template>
	<xsl:template match="modifier[ancestor::fieldsynopsis][text()='const']" mode="python" />

	<xsl:template match="classsynopsisinfo" mode="python">
		<xsl:apply-templates mode="python" />
	</xsl:template>

	<xsl:template match="ooclass|oointerface|ooexception" mode="python">
		<xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
		<span class="{name(.)}"><xsl:apply-templates mode="python" /></span>
	</xsl:template>

	<xsl:template match="modifier" mode="python">
		<span class="{name(.)}"><xsl:apply-templates mode="python" /><xsl:text>&nbsp;</xsl:text></span>
	</xsl:template>

	<xsl:template match="classname" mode="python">
		<xsl:if test="name(preceding-sibling::*[1]) = 'classname'"><xsl:text>, </xsl:text></xsl:if>
		<span class="{name(.)}"><xsl:apply-templates mode="python" /></span>
	</xsl:template>

	<xsl:template match="interfacename" mode="python">
		<xsl:if test="name(preceding-sibling::*[1]) = 'interfacename'"><xsl:text>, </xsl:text></xsl:if>
		<span class="{name(.)}"><xsl:apply-templates mode="python" /></span>
	</xsl:template>

	<xsl:template match="exceptionname" mode="python">
		<xsl:if test="name(preceding-sibling::*[1]) = 'exceptionname'"><xsl:text>, </xsl:text></xsl:if>
		<span class="{name(.)}"><xsl:apply-templates mode="python" /></span>
	</xsl:template>

	<xsl:template match="type" mode="python">
		<span class="{name(.)}"><xsl:apply-templates mode="python" /><xsl:text>&nbsp;</xsl:text></span>
	</xsl:template>

	<xsl:template match="varname" mode="python">
		<strong class="{name(.)}"><xsl:apply-templates mode="python" /></strong>
	</xsl:template>

	<xsl:template match="initializer" mode="python">
		<span class="{name(.)}"><xsl:text>=</xsl:text><xsl:apply-templates mode="python" /></span>
	</xsl:template>

	<xsl:template match="void" mode="python">
		<span class="{name(.)}"><xsl:text>null&nbsp;</xsl:text></span>
	</xsl:template>

	<xsl:template match="methodname" mode="python">
		<strong class="{name(.)}"><xsl:apply-templates mode="python" /></strong>
	</xsl:template>

	<xsl:template match="methodparam" mode="python">
		<xsl:if test="@choice = 'opt' and not(preceding-sibling::*[@choice='opt'])"><xsl:value-of select="$arg.choice.opt.open.str" /></xsl:if>
		<xsl:if test="position() &gt; 1"><xsl:text>, </xsl:text></xsl:if>
		<span class="{name(.)}">
			<xsl:apply-templates mode="python" />
			<xsl:if test="@rep = 'repeat'"><xsl:value-of select="$arg.rep.repeat.str" /></xsl:if>
		</span>
		<xsl:if test="@choice = 'opt' and not(following-sibling::*[@choice='opt'])"><xsl:value-of select="$arg.choice.opt.close.str" /></xsl:if>
	</xsl:template>

	<xsl:template match="parameter" mode="python">
		<var class="{name(.)}"><xsl:apply-templates mode="python" /></var>
	</xsl:template>

</xsl:stylesheet>

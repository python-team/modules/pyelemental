/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYELEMENTAL__VALUE_HH
#define PYELEMENTAL__VALUE_HH

#include "misc.hh"
#include <libelemental/value.hh>

namespace pyElemental {

using Glib::ustring;

bool init_value (PyObject* module);

//******************************************************************************

class color
:	public CxxWrapper<Elemental::color>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static PyGetSetDef get_set[];
	static PyMethodDef methods[];

	static int init (pytype* self, PyObject* args, PyObject* kwargs);

	static PyObject* get_red (pytype* self, void*);
	static int set_red (pytype* self, PyObject* value, void*);

	static PyObject* get_green (pytype* self, void*);
	static int set_green (pytype* self, PyObject* value, void*);

	static PyObject* get_blue (pytype* self, void*);
	static int set_blue (pytype* self, PyObject* value, void*);

	static PyObject* get_luminance (pytype* self, void*);
	static PyObject* get_compliment (pytype* self, void*);
	static PyObject* get_hex_spec (pytype* self, void*);

	static PyObject* composite (pytype* self, PyObject* args);
};

//******************************************************************************

class EntriesView
:	public CxxWrapperBase<Elemental::EntriesView>
{
public:
	
	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	class Unwrapper;

	static PyMethodDef methods[];
	
	static PyObject* create (PyTypeObject* type, PyObject*, PyObject*);

	static PyObject* header (pytype* self, PyObject* args);
	static PyObject* entry (pytype* self, PyObject* args);
};

//******************************************************************************

class EntriesStream
:	public CxxWrapperBase<Elemental::EntriesStream>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static PyObject* wrap (const cxxtype& source);

private:

	static int init (pytype* self, PyObject* args, PyObject* kwargs);
};

//******************************************************************************

class value_base
:	public CxxWrapper<Elemental::value_base>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;
	
	static int set_qualifier (PyObject* self, PyObject* value, void*);

private:

	static PyGetSetDef get_set[];
	static PyMethodDef methods[];

	static int compare (pytype* self, pytype* other);
	static PyObject* str (pytype* self);

	static PyObject* get_has_value (pytype* self, void*);
	static PyObject* get_qualifier (pytype* self, void*);
	static PyObject* get_tip (pytype* self, void*);

	static PyObject* get_string (pytype* self, PyObject* args);
	static PyObject* make_entry (pytype* self, PyObject* args);
};

//******************************************************************************

class color_value_base
:	public CxxWrapper<Elemental::color_value_base>
{
public:

	static bool ready (PyObject* module);
	static PyTypeObject type;

private:

	static PyGetSetDef get_set[];

	static PyObject* get_color (pytype* self, void*);
};

} // namespace pyElemental

#endif // PYELEMENTAL__VALUE_HH

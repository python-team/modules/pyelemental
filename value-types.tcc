/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PYELEMENTAL__VALUE_TYPES_TCC
#define PYELEMENTAL__VALUE_TYPES_TCC

#ifndef PYELEMENTAL__VALUE_TYPES_HH
#error "value-types.tcc must be included from value-types.hh."
#endif

#include <stdexcept>

namespace pyElemental {


//******************************************************************************
// class ValueType<>


ValueType_t (bool)::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, const_cast<char*> (info.name),
			X_PYOBJECT (&type)) == 0;
}


ValueType_t (PyObject*)::wrap (const cxxtype& source)
{
	return wrap_copy (&type, source);
}


ValueType_t (PyTypeObject)::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	const_cast<char*> (info.longname),
	sizeof (pytype), 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT,
	const_cast<char*> (info.description),
	0, 0, 0, 0, 0, 0,
	0, 0, get_set,
	info.pyparent, 0, 0, 0, 0, initproc (init), 0, newfunc (wrapper::create),
	0, 0, 0, 0, 0, 0, 0, 0
};


ValueType_t (PyGetSetDef)::get_set[] =
{
	{ "value", getter (get_value), setter (set_value),
		"The value, if defined.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }\
};


ValueType_t (int)::init (pytype* self, PyObject* args, PyObject* kwargs)
{
	PyObject *value = NULL, *qual = NULL;
	static char *kwlist[] = { "value", "qualifier", NULL };

	if (!PyArg_ParseTupleAndKeywords (args, kwargs, "|OO", kwlist,
		&value, &qual))
		return -1;

	if (value != NULL)
	{
		if (set_value (self, value, NULL) < 0) return -1;
		if (qual == NULL)
			self->cxxobj->qualifier = Elemental::Q_NEUTRAL;
	}

	if (qual != NULL)
		if (value_base::set_qualifier (X_PYOBJECT (self), qual, NULL) < 0)
			return -1;

	return 0;
}


ValueType_t (PyObject*)::get_value (pytype* self, void*)
{
	return info.get_transform (self->cxxobj->value);
}


ValueType_t (int)::set_value (pytype* self, PyObject* value, void*)
{
	if (!X_PyObject_CheckAttr (value, info.pyvaltype, "value", &type))
		return -1;

	try
	{
		self->cxxobj->value = info.set_transform (value);
		return 0;
	}
	catch (...)
	{
		PyErr_Format (PyExc_ValueError, "The given %s is not a valid %s value.",
			info.pyvaltype->tp_name, type.tp_name);
		return -1;
	}
}


//******************************************************************************
// class EnumValueType<>


template<typename cxxtype_,
	const ValueTypeInfo<long, typename cxxtype_::Value> &info>
bool
EnumValueType<cxxtype_, info>::ready (PyObject* module)
{
	return parent::ready (module);
}


template<typename cxxtype_,
	const ValueTypeInfo<long, typename cxxtype_::Value> &info>
bool
EnumValueType<cxxtype_, info>::add_value (char* name,
	typename cxxtype_::Value value)
{
	return X_PyType_AddIntConstant (&parent::type, name, long (value));
}


//******************************************************************************
// class ValueListType<>


ValueListType_t (bool)::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, const_cast<char*> (info.name),
			X_PYOBJECT (&type)) == 0;
}


ValueListType_t (PyObject*)::wrap (const cxxtype& source)
{
	return wrap_copy (&type, source);
}


ValueListType_t (PyTypeObject)::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	const_cast<char*> (info.longname),
	sizeof (pytype), 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT,
	const_cast<char*> (info.description),
	0, 0, 0, 0, 0, 0,
	0, 0, get_set,
	info.pyparent, 0, 0, 0, 0, initproc (init), 0, newfunc (wrapper::create),
	0, 0, 0, 0, 0, 0, 0, 0
};


ValueListType_t (PyGetSetDef)::get_set[] =
{
	{ "values", getter (get_values), setter (set_values),
		"The values, if defined.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }\
};


ValueListType_t (int)::init (pytype* self, PyObject* args, PyObject* kwargs)
{
	PyObject *values = NULL, *qual = NULL;
	static char *kwlist[] = { "values", "qualifier", NULL };

	if (!PyArg_ParseTupleAndKeywords (args, kwargs, "|OO", kwlist,
		&values, &qual))
		return -1;

	if (values != NULL)
	{
		if (set_values (self, values, NULL) < 0) return -1;
		if (qual == NULL)
			self->cxxobj->qualifier = Elemental::Q_NEUTRAL;
	}

	if (qual != NULL)
		if (value_base::set_qualifier (X_PYOBJECT (self), qual, NULL) < 0)
			return -1;

	return 0;
}


ValueListType_t (PyObject*)::get_values (pytype* self, void*)
{
	PyObject *values = PyList_New (self->cxxobj->values.size ());
	if (values == NULL) return NULL;

	int n = 0;
	for (typename std::vector<get_type>::const_iterator
		iter = self->cxxobj->values.begin ();
		iter != self->cxxobj->values.end (); ++iter)
	{
		PyObject *item = info.get_transform (*iter);
		if (item != NULL)
			PyList_SetItem (values, n++, item);
	}

	return values;
}


ValueListType_t (int)::set_values (pytype* self, PyObject* values, void*)
{
	if (values == NULL)
	{
		PyErr_Format (PyExc_TypeError, "cannot delete %s values",
			info.longname);
		return -1;
	}
	if (!X_PySequence_CheckItems (values, info.pyvaltype))
	{
		PyErr_Format (PyExc_TypeError, "%s values must be %s.",
			info.longname, info.pyvaltype->tp_name);
		return -1;
	}
	
	self->cxxobj->values.clear ();
	int size = PySequence_Size (values);
	for (int i = 0; i < size; ++i)
	{
		PyObject *item = PySequence_GetItem (values, i);
		if (item == NULL) continue;
		
		try
			{ self->cxxobj->values.push_back (info.set_transform (item)); }
		catch (...)
		{
			Py_DECREF (item);
			PyErr_Format (PyExc_ValueError,
				"The given %s is not a valid %s value.",
				info.pyvaltype->tp_name, info.longname);
			return -1;
		}
		
		Py_DECREF (item);
	}
	
	return 0;
}


} // namespace pyElemental

#endif // PYELEMENTAL__VALUE_TYPES_TCC

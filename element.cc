/*
 * This file is part of pyElemental, a periodic table Python module with
 * detailed information on elements.
 *
 * Copyright (C) 2006-2007 Kevin Daughtridge <kevin@kdau.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses/>.
 */

#include "element.hh"

namespace pyElemental {


bool
init_element (PyObject* module)
{
	PyObject *pyobj = NULL, *categories = PyList_New (8);
	int n = 0;

#define ADD_PROPERTY(NAME) \
(pyobj = Property::wrap (Elemental::P_##NAME)) &&\
PyModule_AddObject (module, "P_"#NAME, pyobj) == 0

#define ADD_CATEGORY(NAME) \
(pyobj = Category::wrap (Elemental::C_##NAME)) &&\
(Py_INCREF (pyobj), true) &&\
PyModule_AddObject (module, "C_"#NAME, pyobj) == 0 &&\
PyList_SetItem (categories, n++, pyobj) == 0

	return
		categories != NULL &&

		Property::ready (module) &&
		FloatProperty::ready (module) &&
		Category::ready (module) &&
		Element::ready (module) &&

		ADD_PROPERTY (NAME) &&
		ADD_PROPERTY (OFFICIAL_NAME) &&
		ADD_PROPERTY (ALTERNATE_NAME) &&
		ADD_PROPERTY (SYMBOL) &&
		ADD_PROPERTY (NUMBER) &&
		ADD_PROPERTY (SERIES) &&
		ADD_PROPERTY (GROUP) &&
		ADD_PROPERTY (PERIOD) &&
		ADD_PROPERTY (BLOCK) &&

		ADD_PROPERTY (DISCOVERY) &&
		ADD_PROPERTY (DISCOVERED_BY) &&
		ADD_PROPERTY (ETYMOLOGY) &&

		ADD_PROPERTY (PHASE) &&
		ADD_PROPERTY (DENSITY_SOLID) &&
		ADD_PROPERTY (DENSITY_LIQUID) &&
		ADD_PROPERTY (DENSITY_GAS) &&
		ADD_PROPERTY (APPEARANCE) &&

		ADD_PROPERTY (MELTING_POINT) &&
		ADD_PROPERTY (BOILING_POINT) &&
		ADD_PROPERTY (FUSION_HEAT) &&
		ADD_PROPERTY (VAPORIZATION_HEAT) &&
		ADD_PROPERTY (SPECIFIC_HEAT) &&
		ADD_PROPERTY (THERMAL_CONDUCTIVITY) &&
		ADD_PROPERTY (DEBYE_TEMPERATURE) &&

		ADD_PROPERTY (ATOMIC_MASS) &&
		ADD_PROPERTY (ATOMIC_VOLUME) &&
		ADD_PROPERTY (ATOMIC_RADIUS) &&
		ADD_PROPERTY (COVALENT_RADIUS) &&
		ADD_PROPERTY (VAN_DER_WAALS_RADIUS) &&
		ADD_PROPERTY (IONIC_RADII) &&

		ADD_PROPERTY (LATTICE_TYPE) &&
		ADD_PROPERTY (SPACE_GROUP) &&
		ADD_PROPERTY (LATTICE_EDGES) &&
		ADD_PROPERTY (LATTICE_ANGLES) &&
		ADD_PROPERTY (LATTICE_VOLUME) &&

		ADD_PROPERTY (CONFIGURATION) &&
		ADD_PROPERTY (OXIDATION_STATES) &&
		ADD_PROPERTY (ELECTRONEGATIVITY) &&
		ADD_PROPERTY (ELECTRON_AFFINITY) &&
		ADD_PROPERTY (FIRST_ENERGY) &&

		ADD_PROPERTY (COLOR) &&
		ADD_PROPERTY (NOTES) &&

		ADD_CATEGORY (GENERAL) &&
		ADD_CATEGORY (HISTORICAL) &&
		ADD_CATEGORY (PHYSICAL) &&
		ADD_CATEGORY (THERMAL) &&
		ADD_CATEGORY (ATOMIC) &&
		ADD_CATEGORY (CRYSTALLOGRAPHIC) &&
		ADD_CATEGORY (ELECTRONIC) &&
		ADD_CATEGORY (MISCELLANEOUS) &&

		PyModule_AddObject (module, "categories", categories) == 0;
}


//******************************************************************************
// class Property


bool
Property::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "Property", X_PYOBJECT (&type)) == 0;
}


PyTypeObject
Property::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.Property",
	sizeof (pytype), 0,
	destructor (dealloc), 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A property of the chemical elements.",
	0, 0, 0, 0, 0, 0,
	methods, 0, get_set,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


PyObject*
Property::wrap (const cxxtype& source)
{
	const Elemental::FloatProperty *float_source =
		dynamic_cast<const Elemental::FloatProperty*> (&source);
	
	if (float_source != NULL)
		return FloatProperty::wrap (*float_source);
	else
		return wrap_ref (&type, source);
}


PyGetSetDef
Property::get_set[] =
{
	{ "name", getter (get_name), NULL,
		"The translated name of the property.", NULL },
	{ "has_format", getter (has_format), NULL,
		"Whether a compose-style format for values is defined.", NULL },
	{ "format", getter (get_format), NULL,
		"A translated compose-style format for values of the property.", NULL },
	{ "description", getter (get_description), NULL,
		"A translated explanatory description of the property.", NULL },
	{ "sources", getter (get_sources), NULL,
		"Citations for the sources of values for the property.", NULL },
	{ "is_colorable", getter (is_colorable), NULL,
		"Whether values of the property have color representations.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }
};


PyMethodDef
Property::methods[] =
{
	{ "make_entry", PyCFunction (make_entry), METH_VARARGS,
		"(view, value) Possibly adds a value for this property to an "
		"EntriesView." },
	{ NULL, NULL, 0, NULL }
};


PyObject*
Property::get_name (pytype* self, void*)
{
	return X_PyUnicode_FromUstring (self->cxxobj->get_name ());
}


PyObject*
Property::has_format (pytype* self, void*)
{
	return PyBool_FromLong (self->cxxobj->has_format ());
}


PyObject*
Property::get_format (pytype* self, void*)
{
	return X_PyUnicode_FromUstring (self->cxxobj->get_format ());
}


PyObject*
Property::get_description (pytype* self, void*)
{
	return X_PyUnicode_FromUstring (self->cxxobj->get_description ());
}


PyObject*
Property::get_sources (pytype* self, void*)
{
	PyObject* sources = PyList_New (self->cxxobj->sources.size ());
	if (sources == NULL) return NULL;

	int n = 0;
	for (std::list<const Elemental::Message*>::const_iterator
		i = self->cxxobj->sources.begin ();
		i != self->cxxobj->sources.end (); ++i)
	{
		PyObject *source = Message::wrap (**i);
		if (source == NULL)
		{
			Py_DECREF (sources);
			return NULL;
		}
		PyList_SetItem (sources, n++, source);
	}

	return sources;
}


PyObject*
Property::is_colorable (pytype* self, void*)
{
	return PyBool_FromLong (self->cxxobj->is_colorable ());
}


PyObject*
Property::make_entry (pytype* self, PyObject* args)
{
	PyObject *view_ = NULL, *value = NULL;
	if (!PyArg_ParseTuple (args, "O!O", &EntriesView::type, &view_, &value))
		return NULL;
	
	EntriesView::cxxtype &view = EntriesView::unwrap (view_);

	if (PyObject_TypeCheck (value, &value_base::type))
		self->cxxobj->make_entry (view, value_base::unwrap (value));
	else if (PyUnicode_Check (value))
		self->cxxobj->make_entry (view, X_PyUnicode_AsUstring (value));
	else {
		PyErr_SetString (PyExc_TypeError,
			"argument 2 must be unicode, Elemental.value_base, or subclass");
		return NULL;
	}

	Py_RETURN_NONE;
}


//******************************************************************************
// class FloatProperty


bool
FloatProperty::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "FloatProperty", X_PYOBJECT (&type)) == 0;
}


PyTypeObject
FloatProperty::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.FloatProperty",
	sizeof (pytype), 0,
	0, 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A floating-point property of the chemical elements.",
	0, 0, 0, 0, 0, 0,
	methods, 0, get_set,
	&Property::type, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


PyObject*
FloatProperty::wrap (const cxxtype& source)
{
	return wrap_ref (&type, source);
}


PyGetSetDef
FloatProperty::get_set[] =
{
	{ "is_scale_valid", getter (is_scale_valid), NULL,
		"Whether the scale of values for this property is valid.", NULL },
	{ "minimum", getter (get_minimum), NULL,
		"The least value of this property for any element.", NULL },
	{ "linear_medium", getter (get_medium), NULL,
		"The value of this property that has a position of 0.5 on a linear "
		"scale.", (void*) false },
	{ "logarithmic_medium", getter (get_medium), NULL,
		"The value of this property that has a position of 0.5 on a "
		"logarithmic scale.", (void*) true },
	{ "maximum", getter (get_maximum), NULL,
		"The greatest value of this property for any element.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }
};


PyMethodDef
FloatProperty::methods[] =
{
	{ "get_scale_position", PyCFunction (get_scale_position), METH_VARARGS,
		"(instance[, logarithmic]) Returns the position on a normalized 0.0 to "
		"1.0 scale for an instance of this property." },
	{ NULL, NULL, 0, NULL }
};


PyObject*
FloatProperty::is_scale_valid (pytype* self, void*)
{
	return PyBool_FromLong (self->cxxobj->is_scale_valid ());
}


PyObject*
FloatProperty::get_minimum (pytype* self, void*)
{
	try
		{ return PyFloat_FromDouble (self->cxxobj->get_minimum ()); }
	catch (std::domain_error& e)
	{
		PyErr_SetString (PyExc_ValueError, e.what ());
		return NULL;
	}
}


PyObject*
FloatProperty::get_medium (pytype* self, bool logarithmic)
{
	try
		{ return PyFloat_FromDouble (self->cxxobj->get_medium (logarithmic)); }
	catch (std::domain_error& e)
	{
		PyErr_SetString (PyExc_ValueError, e.what ());
		return NULL;
	}
}


PyObject*
FloatProperty::get_maximum (pytype* self, void*)
{
	try
		{ return PyFloat_FromDouble (self->cxxobj->get_maximum ()); }
	catch (std::domain_error& e)
	{
		PyErr_SetString (PyExc_ValueError, e.what ());
		return NULL;
	}
}


PyObject*
FloatProperty::get_scale_position (pytype* self, PyObject* args)
{
	PyObject *instance = NULL;
	int logarithmic = int (false);
	
	if (!PyArg_ParseTuple (args, "O!|i", &Float::type, &instance, &logarithmic))
		return NULL;

	try
	{
		return PyFloat_FromDouble (self->cxxobj->get_scale_position
			(Float::unwrap (instance), logarithmic));
	}
	catch (std::logic_error& e)
	{
		PyErr_SetString (PyExc_ValueError, e.what ());
		return NULL;
	}
}


//******************************************************************************
// class Category


bool
Category::ready (PyObject* module)
{
	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "Category", X_PYOBJECT (&type)) == 0;
}


PyTypeObject
Category::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.Category",
	sizeof (pytype), 0,
	destructor (dealloc), 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A category of properties of the chemical elements.",
	0, 0, 0, 0, 0, 0,
	methods, 0, get_set,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


PyObject*
Category::wrap (const cxxtype& source)
{
	return wrap_ref (&type, source);
}


PyGetSetDef
Category::get_set[] =
{
	{ "name", getter (get_name), NULL,
		"The translated name of the category.", NULL },
	{ "properties", getter (get_properties), NULL,
		"The properties in the category.", NULL },
	{ NULL, NULL, NULL, NULL, NULL }
};


PyMethodDef
Category::methods[] =
{
	{ "make_header", PyCFunction (make_header), METH_VARARGS,
		"(view) Adds the category name to an EntriesView." },
	{ NULL, NULL, 0, NULL }
};


PyObject*
Category::get_name (pytype* self, void*)
{
	return X_PyUnicode_FromUstring (self->cxxobj->get_name ());
}


PyObject*
Category::get_properties (pytype* self, void*)
{
	PyObject* properties = PyList_New (self->cxxobj->properties.size ());
	if (properties == NULL) return NULL;

	int n = 0;
	for (std::list<Elemental::PropertyBase*>::const_iterator
		i = self->cxxobj->properties.begin ();
		i != self->cxxobj->properties.end (); ++i)
	{
		PyObject* property = Property::wrap (**i);
		if (property == NULL)
		{
			Py_DECREF (properties);
			return NULL;
		}
		PyList_SetItem (properties, n++, property);
	}

	return properties;
}


PyObject*
Category::make_header (pytype* self, PyObject* args)
{
	PyObject *view = NULL;
	if (!PyArg_ParseTuple (args, "O!", &EntriesView::type, &view))
		return NULL;
	
	self->cxxobj->make_header (EntriesView::unwrap (view));
	Py_RETURN_NONE;
}


//******************************************************************************
// class Element


bool
Element::ready (PyObject* module)
{
	for (PyGetSetDef *def = get_set; def != NULL && def->name != NULL; ++def)
		if (def->closure != NULL && def->doc == NULL)
			try
			{
				std::string doc = Glib::locale_from_utf8
					(reinterpret_cast<const Elemental::PropertyBase*>
						(def->closure)->get_description ());
				def->doc = g_strdup (doc.data ());
			}
			catch (...) {}

	return PyType_Ready (&type) == 0 &&
		PyModule_AddObject (module, "Element", X_PYOBJECT (&type)) == 0;
}


PyTypeObject
Element::type =
{
	PyObject_HEAD_INIT (NULL) 0,
	"Elemental.Element",
	sizeof (pytype), 0,
	destructor (dealloc), 0, 0, 0, 0, 0,
	0, 0, 0,
	0, 0, 0, 0, 0,
	0,
	Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
	"A chemical element.",
	0, 0, 0, 0, 0, 0,
	methods, 0, get_set,
	0, 0, 0, 0, 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0, 0
};


PyObject*
Element::wrap (const cxxtype& source)
{
	return wrap_ref (&type, source);
}


#define PROPERTY_GETSET(PROP, prop, cxxtype)\
{ #prop, getter (get_property<cxxtype>), NULL, NULL, &Elemental::P_##PROP }


PyGetSetDef
Element::get_set[] =
{
	PROPERTY_GETSET (NAME, name, Message),
	PROPERTY_GETSET (OFFICIAL_NAME, official_name, String),
	PROPERTY_GETSET (ALTERNATE_NAME, alternate_name, String),
	{ "symbol", getter (get_symbol), NULL, NULL, &Elemental::P_SYMBOL },
	{ "number", getter (get_number), NULL, NULL, &Elemental::P_NUMBER },
	PROPERTY_GETSET (SERIES, series, Series),
	PROPERTY_GETSET (GROUP, group, Int),
	PROPERTY_GETSET (PERIOD, period, Int),
	PROPERTY_GETSET (BLOCK, block, Block),

	PROPERTY_GETSET (DISCOVERY, discovery, Event),
	PROPERTY_GETSET (DISCOVERED_BY, discovered_by, Message),
	PROPERTY_GETSET (ETYMOLOGY, etymology, Message),

	PROPERTY_GETSET (PHASE, phase, Phase),
	PROPERTY_GETSET (DENSITY_SOLID, density_solid, Float),
	PROPERTY_GETSET (DENSITY_LIQUID, density_liquid, Float),
	PROPERTY_GETSET (DENSITY_GAS, density_gas, Float),
	PROPERTY_GETSET (APPEARANCE, appearance, Message),

	PROPERTY_GETSET (MELTING_POINT, melting_point, Float),
	PROPERTY_GETSET (BOILING_POINT, boiling_point, Float),
	PROPERTY_GETSET (FUSION_HEAT, fusion_heat, Float),
	PROPERTY_GETSET (VAPORIZATION_HEAT, vaporization_heat, Float),
	PROPERTY_GETSET (SPECIFIC_HEAT, specific_heat, Float),
	PROPERTY_GETSET (THERMAL_CONDUCTIVITY, thermal_conductivity, Float),
	PROPERTY_GETSET (DEBYE_TEMPERATURE, debye_temperature, Float),

	PROPERTY_GETSET (ATOMIC_MASS, atomic_mass, Float),
	PROPERTY_GETSET (ATOMIC_VOLUME, atomic_volume, Float),
	PROPERTY_GETSET (ATOMIC_RADIUS, atomic_radius, Float),
	PROPERTY_GETSET (COVALENT_RADIUS, covalent_radius, Float),
	PROPERTY_GETSET (VAN_DER_WAALS_RADIUS, van_der_waals_radius, Float),
	PROPERTY_GETSET (IONIC_RADII, ionic_radii, String),

	PROPERTY_GETSET (LATTICE_TYPE, lattice_type, LatticeType),
	PROPERTY_GETSET (SPACE_GROUP, space_group, Int),
	PROPERTY_GETSET (LATTICE_EDGES, lattice_edges, FloatList),
	PROPERTY_GETSET (LATTICE_ANGLES, lattice_angles, FloatList),
	PROPERTY_GETSET (LATTICE_VOLUME, lattice_volume, Float),

	PROPERTY_GETSET (CONFIGURATION, configuration, String),
	PROPERTY_GETSET (OXIDATION_STATES, oxidation_states, IntList),
	PROPERTY_GETSET (ELECTRONEGATIVITY, electronegativity, Float),
	PROPERTY_GETSET (ELECTRON_AFFINITY, electron_affinity, Float),
	PROPERTY_GETSET (FIRST_ENERGY, first_energy, Float),

	PROPERTY_GETSET (COLOR, color, ColorValue),
	PROPERTY_GETSET (NOTES, notes, Message),

	{ NULL, NULL, NULL, NULL, NULL }
};


PyMethodDef
Element::methods[] =
{
	{ "get_phase", PyCFunction (get_phase), METH_VARARGS,
		"([tempK]) Returns the phase of matter assumed by the element at "
		"standard pressure and a given temperature." },
	{ "make_entries", PyCFunction (make_entries), METH_VARARGS | METH_KEYWORDS,
		"(view[, category][, all]) Generates entries for all properties or for "
		"the properties in the given category." },
	{ NULL, NULL, 0, NULL }
};


template<typename proptype>
PyObject*
Element::get_property (PyObject* self_, void* prop_)
{
	pytype *self = reinterpret_cast<pytype*> (self_);
	const Elemental::Property<typename proptype::cxxtype> *prop =
		(const Elemental::Property<typename proptype::cxxtype>*) prop_;
	try
		{ return proptype::wrap (self->cxxobj->get_property (*prop)); }
	catch (std::invalid_argument& e)
	{
		PyErr_SetString (PyExc_AttributeError, e.what ());
		return NULL;
	}
}


PyObject*
Element::get_symbol (pytype* self, void*)
{
	return X_PyString_FromCxxString (self->cxxobj->symbol);
}


PyObject*
Element::get_number (pytype* self, void*)
{
	return PyInt_FromLong (self->cxxobj->number);
}


PyObject*
Element::get_phase (pytype* self, PyObject* args)
{
	double temperature = Elemental::STANDARD_TEMPERATURE;
	if (!PyArg_ParseTuple (args, "|d", &temperature)) return NULL;
	return Phase::wrap (self->cxxobj->get_phase (temperature));
}


PyObject*
Element::make_entries (pytype* self, PyObject* args, PyObject* kwargs)
{
	PyObject *view_ = NULL, *category = NULL;
	int all = int (false);

	static char *kwlist[] = { "view", "category", "all", NULL };
	if (!PyArg_ParseTupleAndKeywords (args, kwargs, "O!|O!i", kwlist,
		&EntriesView::type, &view_, &Category::type, &category, &all))
		return NULL;
	
	EntriesView::cxxtype &view = EntriesView::unwrap (view_);

	if (category != NULL)
		self->cxxobj->make_entries
			(view, Category::unwrap (category), bool (all));
	else
		self->cxxobj->make_entries (view);

	Py_RETURN_NONE;
}


} // namespace pyElemental

#!/usr/bin/env python

from distutils.core import setup, Extension
import os, sys

if not sys.version_info[0:2] >= (2, 4):
	raise SystemExit ('Error: Python version >= 2.4 not found')

def pkg_config (opt, pkg):
	fo = os.popen ('pkg-config %s %s' % (opt, pkg))
	return fo.read (), fo.close ()

def pkg_config_version_check (pkg, version):
	output, status = pkg_config ('--atleast-version=%s' % version, pkg)
	if status is None:
		print '%s version >= %s detected' % (pkg, version)
	else:
		raise SystemExit ('Error: %s version >= %s not found' % (pkg, version))

def pkg_config_parse (opt, pkg):
    output, status = pkg_config (opt, pkg)
    opt = opt[-2:]
    return [x.lstrip (opt) for x in output.split ()]

pkg_config_version_check ('libelemental', '1.2.0')

Elemental = Extension ('Elemental',
	[
		'misc.cc',
		'value.cc',
		'value-types.cc',
		'element.cc',
		'table.cc'
	],
	depends=[
		'misc.hh',
		'value.hh',
		'value-types.hh', 'value-types.tcc',
		'element.hh',
		'table.hh'
	],
	include_dirs = pkg_config_parse ('--cflags-only-I', 'libelemental'),
	library_dirs = pkg_config_parse ('--libs-only-L', 'libelemental'),
	libraries = pkg_config_parse ('--libs-only-l', 'libelemental'),
	runtime_library_dirs = pkg_config_parse ('--libs-only-L', 'libelemental')
)

setup (name='pyelemental', version='1.2.0', license='GPL 3+',
	description='binding for libelemental',
	long_description='pyElemental is a periodic table module with detailed ' +
		'information on elements.',
	url='http://www.kdau.com/projects/gelemental/',
	author='Kevin Daughtridge', author_email='kevin+gelemental@kdau.com',
	ext_modules=[Elemental]
)
